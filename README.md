# Project to test NumVerify API using Serenity and RestAssured

A setup for testing the web api of NumVerify using RestAssured and Serenity framework
This purpose of the project is to create automated checks for some functionalities of this API.

The documentation about this API is available here: https://numverify.com/documentation

> _Disclaimer_
>
> _Please note that automation tests provided within this assignment cover only a few test cases out the many there are.
> The main reason of the automation assignment is to demonstrate the ability to automate tests, code styling, approach, architectural solution, along with running tests and generating reports in a pipeline using GitLab pipeline._
>

## Requirements
* Java JDK 11+
* Maven

## How to run the tests

Once the project is cloned on your local machine, execute `mvn  install` to download the dependencies.
This will download:
* `logback-classic`: logging framework for Java applications
* `serenity-core`, `serenity-junit`, `serenity-rest-assured`, `serenity-cucumber6`: The Serenity dependencies
* `rest-assured`: Java DSL for easy testing of REST services
* `json-path`: Used for easily getting values from an Object (Rest Response Body).
* `junit`, `assertj-core`, `org.hamcrest`: Testing framework and for assertions

The automated checks can run with:
* Standalone from the feature files where you can run one Scenario or all the Scenarios in a feature 
* Command line using `mvn verify` 

### Test results

In this project, Serenity framework generates a test report by running the following command `mvn serenity:aggregate`.
The HTML report `index.html` will be generated under `target/site/serenity` folder.

## End-Points

Validating a phone number by providing the phone number and access key (the two mandatory fields):

````
https://apilayer.net/api/validate
? access_key = YOUR_ACCESS_KEY
& number = PHONE_NUMBER
````

Validating a phone number by providing the mandatory fields and country code as optional field:

````
https://apilayer.net/api/validate
? access_key = YOUR_ACCESS_KEY
& number = PHONE_NUMBER
& country_code = COUNTRY_CODE`
````


An example of a valid response:

````
{
    "valid": true,
    "number": "14158586273",
    "local_format": "4158586273",
    "international_format": "+14158586273",
    "country_prefix": "+1",
    "country_code": "US",
    "country_name": "United States of America",
    "location": "Novato",
    "carrier": "AT&T Mobility LLC",
    "line_type": "mobile"
}     
````

And example of an error response:

````
{
    "success": false,
    "error": {
    "code": 210,
    "type": "no_phone_number_provided",
    "info": "Please specify a phone number. [Example: 14158586273]"    
    }
}
````



## Project Structure (Folders and files)

### src/test/java/starter

#### numverify

##### clients
In this folder there is a class called NumVerifyAPI which contains the request specification builders and the rest calls.

##### models
In this folder exist the data entities of the objects.
These objects are used in the deserialization of the responses along with creating the json payloads for the rest clients.

##### stepdefs
In this folder belong the step definitions that translate the Feature Files into code

#### utils
Here reside classes that provide common functionalities. 
In this class I have added a custom assertion that compares actual and expected results in both cases, when values are null or not.

#### CucumberTestSuite Class
This class defines the Cucumber runner. 
By adding the RunWith annotation, the tests will run with the Cucumber runner class.

#### resources

##### features/numverify
This is a resources directory in which the feature files live, and this is a Java Cucumber convention that is used. 
In this directory, you can see two feature files, each contains a different number of scenarios

## Design patterns
* RequestSpecification builder


## Pipeline

This project uses GitLab pipeline to run the all the tests.
The yaml file for this pipeline is available at https://gitlab.com/marwahatoum/numverify/-/blob/main/.gitlab-ci.yml.
The pipeline is triggered as soon as a new commit is pushed.
The test results can be seen in the job run and are attached as an artifact that can be downloaded from the summary of the workflow.

> _Disclaimer_
>
> _Some examples in the verify_numbers_erros.feature feature file were commented out because they were failing.
> The documentation mentions that an error should be generated when the phone number is not numeric.
> However, when adding alphanumeric numbers or numbers with special characters, the request does not fail as the numeric part is only considered.
> While the intended behavior is not explicit, I decided to leave these cases out for the moment._
>