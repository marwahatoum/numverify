@Verify_Numbers
Feature: NumVerify is a RESTful JSON API for national and international phone number validation and information lookup

  Scenario Outline: User calls web service to validate a phone number using only required parameters
    When a user validates the phone number: "<phoneNumber>"
    Then the status code is 200
    And the response includes the following
      | valid                | true                         |
      | number               | 31652600646                  |
      | local_format         | 0652600646                   |
      | international_format | +31652600646                 |
      | country_prefix       | +31                          |
      | country_code         | NL                           |
      | country_name         | Netherlands (Kingdom of the) |
      | location             | null                         |
      | carrier              | Vodafone Libertel BV         |
      | line_type            | mobile                       |
    And the number shows in the right format as "<phoneNumberFormat>"
    Examples:
      | phoneNumber     | phoneNumberFormat |
      | 31652600646     | 31652600646       |
      | +31652600646    | 31652600646       |
      | +31(0)652600646 | 310652600646      |


  Scenario: User calls web service to validate an international phone number using required parameters and one optional parameter
    When a user validates the phone number: "9892508138" and provides "US" as country code
    Then the status code is 200
    And the number shows in the right format as "19892508138"
    And the response includes the following
      | valid                | true                     |
      | number               | 19892508138              |
      | local_format         | 9892508138               |
      | international_format | +19892508138             |
      | country_prefix       | +1                       |
      | country_code         | US                       |
      | country_name         | United States of America |
      | location             | Atlanta                  |
      | carrier              | null                     |
      | line_type            | landline                 |

  Scenario: User calls web service to validate a non-existing number
    When a user validates the phone number: "123456"
    Then the status code is 200
    And the response indicates that the field "valid" is "false"