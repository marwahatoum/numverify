Feature: The API will return a 3-digit error-code,
  an internal error type
  and a plain text "info" object containing suggestions for the user

  Scenario: User gets an error when validating a phone number without access key
    When a user validates the phone number: "+31623467455" without access key
    Then the error response includes the following
      | success | false                                                                                  |
      | code    | 101                                                                                    |
      | type    | missing_access_key                                                                     |
      | info    | You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY] |

  Scenario: User gets an error when validating a phone number with invalid access key
    When a user validates the phone number: "+31623467455" with an invalid access key of value "1as2d487q97ewq1d8f"
    Then the error response includes the following
      | success | false                                                                                   |
      | code    | 101                                                                                     |
      | type    | invalid_access_key                                                                      |
      | info    | You have not supplied a valid API Access Key. [Technical Support: support@apilayer.com] |

  Scenario: User gets an error when validating a phone number with an empty access key
    When a user validates the phone number: "+31623467455" with an invalid access key of value ""
    Then the error response includes the following
      | success | false                                                                                  |
      | code    | 101                                                                                    |
      | type    | missing_access_key                                                                     |
      | info    | You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY] |

  Scenario: User gets an error when no providing a phone number
    When a user validates the phone number: ""
    Then the error response includes the following
      | success | false                                                                                  |
      | code    | 210                                                                                    |
      | type    | no_phone_number_provided                                                               |
      | info    | Please specify a phone number. [Example: 14158586273]                                  |

  Scenario Outline: User gets an error when providing a non numerical phone number
    When a user validates the phone number: "<phoneNumber>"
    Then the error response includes the following
      | success | false                                                         |
      | code    | 211                                                           |
      | type    | non_numeric_phone_number_provided                             |
      | info    | Please specify a numeric phone number. [Example: 14158586273] |
    Examples:
      | phoneNumber  |
      | asdjash      |
#      | qwe54564     |
#      | 465565asdwe  |
#      | ?#@$sdash234 |
#      | 232@#$       |
      | !@#$%^&*     |

  Scenario Outline: User gets an error when providing an invalid country code
    When a user validates the phone number: "9892508138" and provides "<countryCode>" as country code
    Then the error response includes the following
      | success | false                                                                                     |
      | code    | 310                                                                                       |
      | type    | invalid_country_code                                                                      |
      | info    | You have specified an invalid Country Code [Required format: 2-letter Code] [Example: GB] |
    Examples:
      | countryCode |
      | ZZ          |
      | AVC         |
      | 12          |
      | ?!          |
      | A           |
      | P1          |