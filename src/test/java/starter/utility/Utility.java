package starter.utility;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Utility {

    public static void AssertResponseFields(String expectedValue, String actualValue) {
        if(expectedValue == null || expectedValue == "null") {
            assertTrue(actualValue.isEmpty());
        } else {
            assertEquals(expectedValue, String.valueOf(actualValue));
        }
    }
}