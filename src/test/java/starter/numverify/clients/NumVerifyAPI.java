package starter.numverify.clients;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class NumVerifyAPI {

    private static String BASE_URL = "http://apilayer.net/";
    private static String BASE_PATH = "/api/validate";
    private static String ACCESS_KEY_KEY = "access_key";
    private static String NUMBER_KEY = "number";
    private static String COUNTRY_CODE_KEY = "country_code";
    private static String ACCESS_KEY_VALUE = "9f3891363e53d21e9b65556e8195dfca";

    @Step("Verify using phone number {0}")
    public void verifyUsingPhoneNumber(String phoneNumber) {
        SerenityRest.given()
                .spec(validateRequestSpec(phoneNumber))
                .get();
    }

    @Step("Verify using phone number {0} and country code {1}")
    public void verifyUsingPhoneNumberAndCountryCode(String phoneNumber, String countryCode) {
        SerenityRest.given()
                .spec(validateRequestSpec(phoneNumber, countryCode))
                .get();
    }

    @Step("Verify with no phone number")
    public void verifyWithNoPhoneNumber() {
        SerenityRest.given()
                .spec(validateRequestSpec())
                .get();
    }

    @Step("Verify using phone number {0} and without an access key")
    public void verifyWithNoAccessKey(String phoneNumber) {
        SerenityRest.given()
                .spec(validateRequestSpec(phoneNumber, false))
                .get();
    }

    @Step("Verify using phone number {0} and with an invalid access key")
    public void verifyWithInvalidAccessKey(String phoneNumber, Boolean defaultAccessKey, String accessKey) {
        SerenityRest.given()
                .spec(validateRequestSpec(phoneNumber, false, accessKey))
                .get();
    }

    private RequestSpecification validateRequestSpec(String phoneNumber) {
        return new RequestSpecBuilder()
                .setBaseUri(BASE_URL)
                .setBasePath(BASE_PATH)
                .addQueryParam(ACCESS_KEY_KEY, ACCESS_KEY_VALUE)
                .addQueryParam(NUMBER_KEY, phoneNumber)
                .build();
    }

    private RequestSpecification validateRequestSpec(String phoneNumber, Boolean defaultAccessKey, String accessKey) {
        if (!defaultAccessKey) {
            return new RequestSpecBuilder()
                    .setBaseUri(BASE_URL)
                    .setBasePath(BASE_PATH)
                    .addQueryParam(ACCESS_KEY_KEY, accessKey)
                    .addQueryParam(NUMBER_KEY, phoneNumber)
                    .build();
        } else {
            return validateRequestSpec(phoneNumber);
        }

    }

    private RequestSpecification validateRequestSpec(String phoneNumber, String countryCode) {
        return new RequestSpecBuilder()
                .setBaseUri(BASE_URL)
                .setBasePath(BASE_PATH)
                .addQueryParam(ACCESS_KEY_KEY, ACCESS_KEY_VALUE)
                .addQueryParam(NUMBER_KEY, phoneNumber)
                .addQueryParam(COUNTRY_CODE_KEY, countryCode)
                .build();
    }

    private RequestSpecification validateRequestSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(BASE_URL)
                .setBasePath(BASE_PATH)
                .addQueryParam(ACCESS_KEY_KEY, ACCESS_KEY_VALUE)
                .build();
    }

    private RequestSpecification validateRequestSpec(String phoneNumber, Boolean isAccessKeyPresent) {
        if (!isAccessKeyPresent) {
            return new RequestSpecBuilder()
                    .setBaseUri(BASE_URL)
                    .setBasePath(BASE_PATH)
                    .addQueryParam(NUMBER_KEY, phoneNumber)
                    .build();
        } else {
            return validateRequestSpec(phoneNumber);
        }
    }


}
