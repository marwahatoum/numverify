package starter.numverify.stepdefs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.numverify.clients.NumVerifyAPI;
import starter.numverify.models.ErrorResponse;
import starter.numverify.models.NumVerifyResponse;
import starter.utility.Utility;

import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class NumVerifyStepDefinitions {
    @Steps
    NumVerifyAPI numVerifyAPI;

    @When("a user validates the phone number: {string}")
    public void a_user_validates_the_phone_number(String phoneNumber) {
        numVerifyAPI.verifyUsingPhoneNumber(phoneNumber);
    }

    @Then("the status code is {int}")
    public void the_status_code_is(Integer expectedStatusCode) {
        restAssuredThat(response -> response.statusCode(expectedStatusCode));
    }

    @Then("the response includes the following")
    public void the_response_includes_the_following(Map<String, String> responseFields) {
        restAssuredThat(response -> response.body(NumVerifyResponse.VALID, equalTo(
                Boolean.valueOf(responseFields.get("valid")))));
        restAssuredThat(response -> response.body(NumVerifyResponse.INTERNATIONAL_FORMAT, equalTo(
                responseFields.get("international_format"))));
        restAssuredThat(response -> response.body(NumVerifyResponse.COUNTRY_PREFIX, equalTo(
                responseFields.get("country_prefix"))));
        restAssuredThat(response -> response.body(NumVerifyResponse.COUNTRY_CODE, equalTo(
                responseFields.get("country_code"))));
        restAssuredThat(response -> response.body(NumVerifyResponse.COUNTRY_NAME, equalTo(
                responseFields.get("country_name"))));
        restAssuredThat(response -> response.body(NumVerifyResponse.LINE_TYPE, equalTo(
                responseFields.get("line_type"))));
    }

    @Then("the number shows in the right format as {string}")
    public void the_number_shows_in_the_right_format_as(String expectedFormat) {
        restAssuredThat(response -> response.body(NumVerifyResponse.NUMBER, equalTo(expectedFormat)));
    }

    @When("a user validates the phone number: {string} and provides {string} as country code")
    public void a_user_validates_the_phone_number_and_provides_as_country_code(String phoneNumber, String countryCode) {
        numVerifyAPI.verifyUsingPhoneNumberAndCountryCode(phoneNumber, countryCode);
    }

    @Then("the response indicates that the field {string} is {string}")
    public void the_response_indicates_that_the_field_is(String field, String expectedValue) {
        String actualValue = SerenityRest.lastResponse().body().jsonPath().get(field).toString();
        Utility.AssertResponseFields(expectedValue, actualValue);
    }

    @When("a user validates the phone number: {string} without access key")
    public void a_user_validates_the_phone_number_without_access_key(String phoneNumber) {
        numVerifyAPI.verifyWithNoAccessKey(phoneNumber);
    }

    @Then("the error response includes the following")
    public void the_error_response_includes_the_following(Map<String, String> responseFields) {
        restAssuredThat(response -> response.body(ErrorResponse.SUCCESS, equalTo(
                Boolean.valueOf(responseFields.get("success")))));
        restAssuredThat(response -> response.body(ErrorResponse.CODE, equalTo(
                Integer.parseInt(responseFields.get("code")))));
        restAssuredThat(response -> response.body(ErrorResponse.TYPE, equalTo(
                responseFields.get("type"))));
        restAssuredThat(response -> response.body(ErrorResponse.INFO, equalTo(
                responseFields.get("info"))));
    }

    @When("a user validates the phone number: {string} with an invalid access key of value {string}")
    public void a_user_validates_the_phone_number_with_an_invalid_access_key_of_value(String phoneNumber, String accessKey) {
        numVerifyAPI.verifyWithInvalidAccessKey(phoneNumber, false, accessKey);
    }
}
