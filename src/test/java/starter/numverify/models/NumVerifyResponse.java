package starter.numverify.models;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class NumVerifyResponse {
    public static final String VALID = "'valid'";
    public static final String NUMBER = "'number'";
    public static final String LOCAL_FORMAT = "'local_format'";
    public static final String INTERNATIONAL_FORMAT = "'international_format'";
    public static final String COUNTRY_PREFIX = "'country_prefix'";
    public static final String COUNTRY_CODE = "'country_code'";
    public static final String COUNTRY_NAME = "'country_name'";
    public static final String LOCATION = "'location'";
    public static final String CARRIER = "'carrier'";
    public static final String LINE_TYPE = "'line_type'";
}
