package starter.numverify.models;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ErrorResponse {
    public static final String SUCCESS = "'success'";
    public static final String CODE = "'error'.'code'";
    public static final String TYPE = "'error'.'type'";
    public static final String INFO = "'error'.'info'";
}
